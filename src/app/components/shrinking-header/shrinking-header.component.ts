import { Component, AfterViewInit, Input, ElementRef, Renderer2, Output, EventEmitter, NgZone } from '@angular/core';
import { DomController } from '@ionic/angular';

@Component({
	selector: 'shrinking-header',
  	templateUrl: './shrinking-header.component.html',
  	styleUrls: ['./shrinking-header.component.scss']
})
export class ShrinkingHeaderComponent implements AfterViewInit {

	@Input('scrollArea') scrollArea: any;
	@Input('headerHeight') headerHeight: any;

	@Output() collapsed = new EventEmitter<boolean>();
	isCollapsed: boolean = false;

	constructor(public element: ElementRef, public renderer: Renderer2, public zone: NgZone, private domCtrl: DomController) {

	}

	ngAfterViewInit(){
		
		
		this.renderer.setStyle(this.element.nativeElement, 'height', this.headerHeight + 'px');

		this.scrollArea.ionScroll.subscribe((ev) => {
			this.resizeHeader(ev);
		});

	}

	resizeHeader(ev){

		let adjustedHeight = this.headerHeight - ev.detail.scrollTop;

		if(adjustedHeight <= 0){

			adjustedHeight = 0;
			
			if(!this.isCollapsed){
				this.isCollapsed = true;
			
				this.zone.run(() => {
					this.collapsed.emit(true);
				});
				
			}

		}

		if(adjustedHeight > 0){

			if(this.isCollapsed){
				this.isCollapsed = false;
				this.zone.run(() => {
					this.collapsed.emit(false);
				});
			}
			
		}

		this.domCtrl.write(() => {
			this.renderer.setStyle(this.element.nativeElement, 'height', adjustedHeight + 'px');
		});

	}

}